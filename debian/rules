#!/usr/bin/make -f
# -*- makefile -*-

#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk

BUILD_DATE  = $(shell LC_ALL=C date -u "+%d %B %Y" -d "@$(SOURCE_DATE_EPOCH)")
SPHINXOPTS := -E -N -D html_last_updated_fmt="$(BUILD_DATE)"

export PYBUILD_NAME=django-tables2
export PYBUILD_AFTER_INSTALL=rm -rf {destdir}/usr/lib/python*/dist-packages/django_tables2/static/django_tables2

%:
	dh $@ --buildsystem=pybuild

override_dh_sphinxdoc:
ifeq (,$(findstring nodoc, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH={build_dir} \
	    python3 -m sphinx -b html $(SPHINXOPTS) docs $(CURDIR)/debian/python-django-tables2-doc/usr/share/doc/python-django-tables2-doc/html
	# docs/conf.py generates a symlink of the CHANGELOG to docs/pages/CHANGELOG
	$(RM) -r $(CURDIR)/debian/python-django-tables2-doc/usr/share/doc/python-django-tables2-doc/html/pages/CHANGELOG
	dh_sphinxdoc
endif

override_dh_installexamples:
	dh_installexamples -A -X__pycache__

override_dh_auto_clean:
	dh_auto_clean
	${RM} -r .cache django_tables2.egg-info
	${RM} docs/pages/CHANGELOG.md

override_dh_auto_test:
	dh_auto_test -- --system=custom --test-args="{interpreter} -Wd manage.py test"
